package controller

import (
	"fmt"
	"ginger/serializer"
	"ginger/service"
	"github.com/gin-gonic/gin"
)

func GetRule(c *gin.Context) {
	return
}

func GetRules(c *gin.Context) {
	var query service.RuleGetsService
	if c.ShouldBindQuery(&query) == nil {
		res := query.List()
		c.JSON(200, res)
	} else {
		c.JSON(200, "error")
	}
}

func PostRules(c *gin.Context) {
	var RuleService service.RuleCreateService
	err := c.ShouldBind(&RuleService)
	fmt.Println(err)
	if err == nil {
		if rule, res := RuleService.Create(); res != nil {
			c.JSON(200, res)
		} else {
			c.JSON(200, serializer.BuildCreateResponse(rule.ID))
		}
	} else {
		c.JSON(200, serializer.Response{
			Code:  40004,
			Data:  err.Error(),
			Msg:   "",
			Error: "",
		})
	}
}

func PutRules(c *gin.Context) {

}

func DeleteRules(c *gin.Context) {

}
