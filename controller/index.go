package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func IndexPage(c *gin.Context) {
	user, _ := c.Get("user")
	c.HTML(http.StatusOK, "index.tmpl", gin.H{
		"user": user,
	})
	return
}
