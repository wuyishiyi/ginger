package controller

import (
	"ginger/model"
	"ginger/serializer"
	"ginger/service"
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm"
)

func SessionCreate(c *gin.Context) {
	var SessionService service.SessionCreateService
	if err := c.ShouldBind(&SessionService); err == nil {
		if user, err := SessionService.Create(); err != nil {
			c.JSON(200, err)
		} else {
			// 设置 Session
			res := serializer.BuildLoginResponse(user.BuildToken(2))
			c.JSON(200, res)
		}
	} else {

	}
}

func GetSession(c *gin.Context) {
	token := c.Request.Header.Get("token")
	user, _ := c.Get("user")
	v, ok := user.(model.User)
	if ok {
		res := serializer.BuildSessionResponse(v, token)
		c.JSON(200, res)
	} else {
		c.JSON(200, serializer.Response{
			Code: 401,
			Msg:  "需要登录",
		})
	}
}
