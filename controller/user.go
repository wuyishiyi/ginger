package controller

import (
	"fmt"
	"ginger/model"
	"ginger/serializer"
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm"
)

func UserRegister(c *gin.Context) {
	user := model.User{
		Account: c.PostForm("account"),
		Email:   c.PostForm("email"),
		Status:  model.Active,
	}
	fmt.Println("password:", c.PostForm("password"))
	// 加密密码
	if err := user.SetPassword(c.PostForm("password")); err != nil {
		c.JSON(200, serializer.Response{
			Code: 40002,
			Msg:  "密码加密失败",
		})
		return
	}
	user.SetAvatar()
	// 创建用户
	if err := model.DB.Create(&user).Error; err != nil {
		c.JSON(200, serializer.Response{
			Code: 40002,
			Msg:  "注册失败",
		})
		return
	}
	res := serializer.BuildUserResponse(user)
	c.JSON(200, res)
	return
}
