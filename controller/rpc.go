package controller

import (
	"ginger/yar"
	"github.com/gin-gonic/gin"
)

func Rpc(c *gin.Context) {
	server := yar.GetServer()
	server.Handle(c.Request)
}
