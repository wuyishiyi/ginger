package service

import (
	"ginger/model"
	"ginger/serializer"
)

type RuleGetsService struct {
	Page     uint   `form:"page"`
	Limit    uint   `form:"limit"`
	ParentId uint   `form:"parent_id"`
	Name     string `form:"name"`
}

// List 权限列表
func (service *RuleGetsService) List() serializer.Response {
	rules := []model.Rule{}
	total := 0

	if service.Limit == 0 {
		service.Limit = 6
	}

	tx := model.DB.Model(model.Rule{})
	if len(service.Name) > 0 {
		tx = tx.Where("name like ?", "%"+service.Name+"%")
	}

	if err := tx.Count(&total).Error; err != nil {
		return serializer.Response{
			Code:  50000,
			Msg:   "数据库连接错误",
			Error: err.Error(),
		}
	}

	if err := tx.Limit(service.Limit).Offset((service.Page - 1) * service.Limit).Find(&rules).Error; err != nil {
		return serializer.Response{
			Code:  50000,
			Msg:   "数据库连接错误",
			Error: err.Error(),
		}
	}

	return serializer.BuildListResponse(serializer.BuildRules(rules), uint(total))
}
