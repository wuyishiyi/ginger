package service

import (
	"ginger/model"
	"ginger/serializer"
	"time"
)

type RuleCreateService struct {
	Name     string `form:"name" json:"name" binding:"required,max=16"`
	ParentId uint   `form:"parent_id" json:"parent_id"`
	Desc     string `form:"desc" json:"desc" `
	Icon     string `form:"icon" json:"desc"`
	Path     string `form:"path" json:"desc"`
	Type     uint   `form:"type" json:"desc"`
}

func (service *RuleCreateService) Create() (model.Rule, *serializer.Response) {
	rule := model.Rule{
		Name:       service.Name,
		ParentId:   service.ParentId,
		Desc:       service.Desc,
		Icon:       service.Icon,
		Path:       service.Path,
		Type:       service.Type,
		UpdateTime: time.Now().Unix(),
		CreateTime: time.Now().Unix(),
	}
	if err := model.DB.Create(&rule).Error; err != nil {
		return rule, &serializer.Response{
			Code: 40003,
			Msg:  "添加权限出错",
		}
	}
	return rule, nil
}
