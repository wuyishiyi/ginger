package service

import (
	"ginger/model"
	"ginger/serializer"
	"github.com/gin-gonic/gin"
	"time"
)

type SessionCreateService struct {
	Account  string `form:"account" json:"account" binding:"required,min=5,max=30"`
	Password string `form:"password" json:"password" binding:"required,min=6,max=40"`
}

func (service *SessionCreateService) Create() (model.User, *serializer.Response) {
	var user model.User
	if err := model.DB.Where("account = ?", service.Account).First(&user).Error; err != nil {
		return user, &serializer.Response{
			Code: 40001,
			Msg:  "用户名错误或不存在",
		}
	}
	if user.CheckPassword(service.Password) == false {
		return user, &serializer.Response{
			Code: 40002,
			Msg:  "用户名错误或密码错误",
		}
	}
	return user, nil
}

func (service *SessionCreateService) CreateSuccess(user model.User, c *gin.Context) error {
	user.LastLoginIp = c.ClientIP()
	user.LastLoginTime = int(time.Now().Unix())
	err := model.DB.Save(&user).Error
	return err
}
