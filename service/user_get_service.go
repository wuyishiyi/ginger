package service

import (
	"ginger/model"
	"ginger/serializer"
	"github.com/gin-gonic/gin"
)

type UserGetService struct {
	Uid uint64 `form:"id" json:"id"`
}

func (service *UserGetService) GetUserInfo(user model.User, c *gin.Context) (model.User, *serializer.Response) {
	//if ()
	return user, nil
}
