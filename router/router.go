package router

import (
	"fmt"
	"ginger/controller"
	"ginger/middleware"
	"ginger/yar"
	"github.com/gin-gonic/gin"
	"os"
)

func init() {

}

func Run() {
	r := gin.Default()
	RpcInit()
	r.Static("/assets", "./assets")
	r.Use(middleware.Session(os.Getenv("SESSION_SECRET")))
	r.Use(middleware.CurrentUser())
	rpc := r.Group("/rpc")
	rpc.POST("common", controller.Rpc)
	loginAuth := r.Group("/")
	loginAuth.Use(middleware.AuthRequired())
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	// 用户登录
	r.POST("user", controller.UserRegister)
	r.POST("session", controller.SessionCreate)
	loginAuth.GET("/", controller.IndexPage)
	loginAuth.GET("session", controller.GetSession)
	r.GET("rules", controller.GetRules)
	r.POST("rules", controller.PostRules)
	err := r.Run(":8000") // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
	if err != nil {
		fmt.Println(err.Error())
	}
}

func RpcInit() {
	server := yar.InitServer()
	server.Register("common", yar.Common)

}
