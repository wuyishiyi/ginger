package model

import (
	"crypto/md5"
	"encoding/hex"
	"ginger/until"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
	"strconv"
	"strings"
	"time"
)

// User 用户模型
type User struct {
	gorm.Model
	Account       string `gorm:"unique_index:account;type:varchar(64)"`
	Email         string `gorm:"unique_index:email;type:varchar(64)"`
	Phone         string `gorm:"unique_index:phone;type:varchar(11)"`
	Password      string
	LastLoginIp   string `gorm:"type:varchar(16)"`
	LastLoginTime int    `gorm:"type:int(11)"`
	Avatar        string
	CreateTime    int
	Status        int `gorm:"type:tinyint(4)"`
	Token         string
}

const (
	// PassWordCost 密码加密难度
	PassWordCost = 12
	// Active 激活用户
	Active int = 1
	// Inactive 未激活用户
	Inactive int = 0
	// Suspend 被封禁用户
	Suspend int = -1
)

// GetUser 用ID获取用户
func GetUser(ID interface{}) (User, error) {
	var user User
	result := DB.First(&user, ID)
	return user, result.Error
}

// SetAvatar 设置密码
func (user *User) SetAvatar() {
	md5d := md5.New()
	md5d.Write([]byte(strings.ToUpper(strings.Trim(user.Email, " "))))
	user.Avatar = "http://www.gravatar.com/avatar/" + hex.EncodeToString(md5d.Sum(nil))
}

// SetPassword 设置密码
func (user *User) SetPassword(password string) error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), PassWordCost)
	if err != nil {
		return err
	}
	user.Password = string(bytes)
	return nil
}

// CheckPassword 校验密码
func (user *User) CheckPassword(password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	return err == nil
}

// 创建 token
func (user *User) BuildToken(_type int64) string {
	sUid := strconv.FormatUint(uint64(user.ID), 10)
	if _type > 3 {
		_type = 0
	}

	timestamp := time.Now().Unix() + (_type+1)*3600
	sTimestamp := strconv.FormatUint(uint64(timestamp), 10)
	tokenKey := sUid + "." + sTimestamp + "." + "type"
	token, err := until.RSAEncrypt([]byte(tokenKey))
	if err != nil {
		panic(err)
	}
	return string(until.Base64Encode(token))
}

// 校验 token
func CheckToken(tokenD string) (User, bool) {
	var user User
	token, err := until.Base64Decode([]byte(tokenD))
	if err == nil {
		sToken, err := until.RSADecrypt(token)
		if err == nil {
			mToken := strings.SplitN(string(sToken), ".", 3)
			user, err = GetUser(mToken[0])
			if err == nil {
				return user, true
			}
		}
	}
	return user, false
}
