package model

// User 用户模型
type Rule struct {
	ID         uint   `gorm:"primary_key;"`
	Name       string `gorm:"type:varchar(64);not null"`
	ParentId   uint   `gorm:"default:0;not null;column:pid;"`
	Desc       string `gorm:"type:text(0);not null"`
	Icon       string `gorm:"default:'';not null"`
	Path       string `gorm:"not null"`
	Type       uint   `gorm:"not null;comment: '1 path 2 menu 3 api'"`
	UpdateTime int64  `gorm:"not null;"`
	CreateTime int64  `gorm:"not null;"`
}
