package model

import "github.com/jinzhu/gorm"

// User 用户模型
type Session struct {
	gorm.Model
	Account       string `gorm:"unique_index:account"`
	Email         string `gorm:"unique_index:account"`
	Phone         string `gorm:"unique_index:account"`
	Password      string
	LastLoginIp   string
	LastLoginTime int
	Avatar        string `gorm:"size:1000"`
	CreateTime    int
	Status        int
	Token         string
}
