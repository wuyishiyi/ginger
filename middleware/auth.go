package middleware

import (
	"ginger/model"
	"ginger/serializer"
	"github.com/gin-gonic/gin"
)

func CurrentUser() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get("token")
		user, isCheck := model.CheckToken(token)
		if isCheck {
			c.Set("user", user)
		}
		c.Set("token", token)
		c.Next()
	}
}

func AuthRequired() gin.HandlerFunc {
	return func(c *gin.Context) {
		user, _ := c.Get("user")
		if user != nil {
			if _, ok := user.(model.User); ok {
				c.Next()
				return
			}
		}
		c.JSON(200, serializer.Response{
			Code: 401,
			Msg:  "需要登录",
		})
		c.Abort()
	}
}
