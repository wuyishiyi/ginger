package main

import (
	"ginger/model"
	"ginger/router"
	"github.com/joho/godotenv"
	"os"
)

func main() {
	// 从本地读取环境变量
	_ = godotenv.Load()

	// 连接数据库
	model.Database(os.Getenv("MYSQL_DSN"))
	router.Run()
}
