package main

import (
	"fmt"
	"ginger/model"
	"ginger/until"
	"github.com/joho/godotenv"
	"os"
)

type testNode struct {
	until.Node
	test string
}

func test_gorm() {

	// 从本地读取环境变量
	godotenv.Load()

	// 连接数据库
	model.Database(os.Getenv("MYSQL_DSN"))

}

func test_goconf() {
	conf := until.ParseConf("test.ini")
	fmt.Println(conf)
	fmt.Println(conf.GetValue("athena_getReadData", "host"))
}

func testTree() {
	var tests []testNode
	test := testNode{}
	test.ID = 1
	test.ParentId = 0
	test.test = "test1"
	tests = append(tests, test)
	test.ID = 2
	test.ParentId = 1
	test.test = "test2"
	tests = append(tests, test)
	trees := make([]until.Tree, 0)
	until.GenerateTree(tests, trees)
}

func main() {
	//pw, _ := bcrypt.GenerateFromPassword([]byte("66666666"), 12)
	//pw := []byte("$2a$12$JgROiYhA7mEIUE8Gx2gDVOclZSHvYvaLLtxklCto9a132.pMejxLG")
	//err := bcrypt.CompareHashAndPassword(pw, []byte("66666666"))
	//if err != nil {
	//	fmt.Println(err)
	//}
	testTree()
}
