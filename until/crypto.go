package until

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"github.com/joho/godotenv"
	"os"
)

const alphabet = "./ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

var publicKey, privatekey []byte

var bcEncoding = base64.NewEncoding(alphabet)

func init() {
	// 从本地读取环境变量
	godotenv.Load()
	publicKey = FileLoad(os.Getenv("PUBLIC_KEY"))
	privatekey = FileLoad(os.Getenv("PRIVATE_KEY"))
}

func RSAEncrypt(orgidata []byte) ([]byte, error) {
	block, _ := pem.Decode(publicKey)
	if block == nil {
		return nil, errors.New("public key is bad")
	}
	pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	pub := pubInterface.(*rsa.PublicKey)
	return rsa.EncryptPKCS1v15(rand.Reader, pub, orgidata) //加密
}

func RSADecrypt(cipertext []byte) ([]byte, error) {
	block, _ := pem.Decode(privatekey)
	if block == nil {
		return nil, errors.New("public key is bad")
	}
	priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	return rsa.DecryptPKCS1v15(rand.Reader, priv, cipertext)
}

func FileLoad(filepath string) ([]byte) {
	fp, err := os.Open(filepath)
	defer fp.Close()
	if err != nil {
		return nil
	}
	privateKey := make([]byte, 2048)
	num, err := fp.Read(privateKey)
	return privateKey[:num]
}

func Base64Encode(src []byte) []byte {
	n := bcEncoding.EncodedLen(len(src))
	dst := make([]byte, n)
	bcEncoding.Encode(dst, src)
	for dst[n-1] == '=' {
		n--
	}
	return dst[:n]
}

func Base64Decode(src []byte) ([]byte, error) {
	numOfEquals := 4 - (len(src) % 4)
	for i := 0; i < numOfEquals; i++ {
		src = append(src, '=')
	}
	dst := make([]byte, bcEncoding.DecodedLen(len(src)))
	n, err := bcEncoding.Decode(dst, src)
	if err != nil {
		return nil, err
	}
	return dst[:n], nil
}
