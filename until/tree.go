package until

type Tree struct {
	Node
	Subject []Tree
}

type Node struct {
	ID       uint
	ParentId uint
}

func GenerateTree(nodes []interface{}, trees []Tree) []Tree {
	tree := Tree{}
	for _, node := range nodes {
		if v, ok := interface{}(node).(Node); ok {
			tree = Tree{
				Node:    v,
				Subject: nil,
			}
		}
		trees = append(trees, tree)
	}
	return trees
}
