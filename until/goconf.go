package until

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

const (
	ConfFileIni = "ini"
)

type Config struct {
	filepath string                       //your ini file path directory+file
	Conflist map[string]map[string]string //configuration information slice
}

type ConfValue struct {
	_type    int
	parent   *ConfValue
	value    string
	dataList map[string]*ConfValue
}

type ConfFile struct {
	filepath string
	conftype string
	dataList map[string]*ConfValue
}

var ConfManage map[string]*ConfFile

func init() {
	ConfManage = make(map[string]*ConfFile)
}

func ParseConf(filepath string) *ConfFile {
	if _, ok := ConfManage[filepath]; ok {
		return ConfManage[filepath]
	} else {
		ConfManage[filepath] = &ConfFile{
			filepath: filepath,
			conftype: ConfFileIni,
			dataList: make(map[string]*ConfValue),
		}
		ConfManage[filepath].parse()
		return ConfManage[filepath]
	}
}

func (conf *ConfFile) parse() map[string]*ConfValue {
	fp, err := os.Open(conf.filepath)
	if err != nil {
		CheckErr(err)
	}
	defer fp.Close()
	var section string
	var _type int // 0 section 1 string 2 int 3 float 4 bool
	var sectionMap = &ConfValue{
		parent:   nil,
		_type:    -1,
		value:    "",
		dataList: make(map[string]*ConfValue),
	}
	isFirstSection := true
	buf := bufio.NewReader(fp)
	for {
		l, err := buf.ReadString('\n')
		line := strings.TrimSpace(l)
		if err != nil {
			if err != io.EOF {
				CheckErr(err)
			}
			if len(line) == 0 {
				break
			}
		}
		switch {
		case len(line) == 0:
		case string(line[0]) == "#": //增加配置文件备注
		case line[0] == '[' && line[len(line)-1] == ']':
			if !isFirstSection {
				conf.dataList[section] = sectionMap
			} else {
				isFirstSection = false
			}
			section = strings.TrimSpace(line[1 : len(line)-1])
			sections := strings.SplitN(line[1:len(line)-1], ":", 2)
			if len(sections) > 1 {
				sectionMap = &ConfValue{
					parent:   conf.dataList[strings.TrimSpace(sections[1])],
					_type:    0,
					value:    "",
					dataList: make(map[string]*ConfValue),
				}
				section = strings.TrimSpace(sections[0])
			} else {
				sectionMap = &ConfValue{
					parent:   nil,
					_type:    0,
					value:    "",
					dataList: make(map[string]*ConfValue),
				}
			}
		default:
			i := strings.IndexAny(line, "=")
			if i == -1 {
				continue
			}
			value := strings.TrimSpace(line[i+1 : len(line)])
			if (value[0:1] == "\"" && value[len(value)-1:] == "\"") || (value[0:1] == "'" && value[len(value)-1:] == "'") {
				value = strings.Trim(value, "'\"")
				_type = 1
			} else {
				if _, err := strconv.ParseInt(value, 0, 64); err != nil {
					_type = 2
				} else if _, err := strconv.ParseFloat(value, 64); err != nil {
					_type = 3
				} else if _, err := strconv.ParseBool(value); err != nil {
					_type = 4
				} else {
					_type = 1
				}
			}
			sectionMap.dataList[strings.TrimSpace(line[0:i])] = &ConfValue{
				_type:    _type,
				parent:   nil,
				value:    value,
				dataList: nil,
			}
		}
	}
	conf.dataList[section] = sectionMap
	return conf.dataList
}

// To obtain corresponding value of the key values
func (conf *ConfFile) GetValue(section, name string) string {
	if section == "" {
		_, ok := conf.dataList[section]
		if ok {
			return conf.dataList[name].Get()
		} else {
			return ""
		}
	} else {
		_, ok := conf.dataList[section]
		if ok {
			return conf.dataList[section].GetValue(name)
		} else {
			return ""
		}
	}
}

func (conf *ConfValue) Get() string {
	if conf._type == 0 {
		return ""
	} else {
		return conf.value
	}
}

func (conf *ConfValue) Set(value string) {
	var _type int;
	if _, err := strconv.ParseInt(value, 0, 64); err != nil {
		_type = 2
	} else if _, err := strconv.ParseFloat(value, 64); err != nil {
		_type = 3
	} else if _, err := strconv.ParseBool(value); err != nil {
		_type = 4
	} else {
		_type = 1
	}
	conf._type = _type
	conf.value = value
}

func (conf *ConfValue) SetValue(key, value string) {
	if _, ok := conf.dataList[key]; !ok {
		conf.dataList[key] = &ConfValue{
			_type:    -1,
			parent:   nil,
			value:    "",
			dataList: nil,
		}
	}
	conf.dataList[key].Set(value)
}

func (conf *ConfValue) GetValue(name string) string {
	if conf._type == 0 {
		if _, ok := conf.dataList[name]; ok {
			return conf.dataList[name].Get()
		} else {
			if conf.parent == nil {
				return ""
			} else {
				return conf.parent.GetValue(name)
			}
		}
	} else {
		return ""
	}
}

func (conf *ConfValue) DeleteValue(name string) bool {
	if _, ok := conf.dataList[name]; ok {
		delete(conf.dataList, name)
		return true
	} else {
		return true
	}
}

func (conf *ConfFile) DeleteValue(section, name string) bool {
	if _, ok := conf.dataList[section]; ok {
		if name == "" {
			delete(conf.dataList, section)
		} else {
			conf.dataList[section].DeleteValue(name)
		}
		return true
	} else {
		return true
	}
}

func (conf *ConfFile) SetValue(section, key, value string) {
	if section == "" {
		if _, ok := conf.dataList[key]; !ok {
			conf.dataList[key] = &ConfValue{
				_type:    -1,
				parent:   nil,
				value:    "",
				dataList: nil,
			}
		}
		conf.dataList[key].SetValue(key, value)
	} else {
		if _, ok := conf.dataList[section]; !ok {
			conf.dataList[section] = &ConfValue{
				_type:    0,
				parent:   nil,
				value:    "",
				dataList: make(map[string]*ConfValue),
			}
		}
		conf.dataList[section].SetValue(key, value)
	}
}

//获取所有配置项
//List all the configuration file
func (conf *ConfFile) GetAllSetion() map[string]*ConfValue {
	return conf.dataList
}

func CheckErr(err error) string {
	if err != nil {
		return fmt.Sprintf("Error is :'%s'", err.Error())
	}
	return "Notfound this error"
}
