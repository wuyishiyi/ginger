package serializer

import "ginger/model"

// User 用户序列化器
type User struct {
	ID          uint   `json:"id"`
	UserName    string `json:"user_name"`
	LastLoginIp string `json:"last_login_ip"`
	Status      int    `json:"status"`
	Avatar      string `json:"avatar"`
	CreatedAt   int64  `json:"created_at"`
}

// UserResponse 单个用户序列化
type UserResponse struct {
	Response
	Data User `json:"data"`
}

type LoginResponse struct {
	Response
	Data string `json:"data"`
}

// BuildUser 序列化用户
func BuildUser(user model.User) User {
	return User{
		ID:        user.ID,
		UserName:  user.Account,
		Status:    user.Status,
		Avatar:    user.Avatar,
		CreatedAt: user.CreatedAt.Unix(),
	}
}

// BuildUserResponse 序列化用户响应
func BuildUserResponse(user model.User) UserResponse {
	return UserResponse{
		Data: BuildUser(user),
	}
}

func BuildLoginResponse(token string) LoginResponse {
	return LoginResponse{
		Data: token,
	}
}
