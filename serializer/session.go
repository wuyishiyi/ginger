package serializer

import "ginger/model"

// User 用户序列化器
type Session struct {
	User  User
	Token string
}

// UserResponse 单个用户序列化
type GetSessionResponse struct {
	Response
	Data Session `json:"data"`
}

type CreateSessionResponse struct {
	Response
	Data string `json:"data"`
}

func BuildSession(user model.User, token string) Session {
	return Session{
		User:  BuildUser(user),
		Token: token,
	}
}

func BuildSessionResponse(user model.User, token string) GetSessionResponse {
	return GetSessionResponse{
		Data: BuildSession(user, token),
	}
}
