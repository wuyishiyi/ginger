package serializer

import "ginger/model"

type Rule struct {
	ID         uint   `json:"id"`
	Name       string `json:"name"`
	ParentId   uint   `json:"parent_id"`
	Desc       string `json:"desc"`
	Icon       string `json:"icon"`
	Path       string `json:"path"`
	Type       uint   `json:"type"`
	UpdateTime int64 `json:"update_time"`
	CreateTime int64 `json:"create_time"`
}

func BuildRule(item model.Rule) Rule {
	return Rule{
		ID:         item.ID,
		Name:       item.Name,
		ParentId:   item.ParentId,
		Desc:       item.Desc,
		Icon:       item.Icon,
		Path:       item.Path,
		Type:       item.Type,
		UpdateTime: item.UpdateTime,
		CreateTime: item.CreateTime,
	}
}

func BuildRules(items []model.Rule) (rules []Rule) {
	for _, item := range items {
		rule := BuildRule(item)
		rules = append(rules, rule)
	}
	return rules
}
