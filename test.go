package main

import (
	"context"
	"fmt"
	"go.etcd.io/etcd/clientv3"
	"google.golang.org/grpc/grpclog"
	"log"
	"os"
	"time"
)

func main() {

	clientv3.SetLogger(grpclog.NewLoggerV2(os.Stderr, os.Stderr, os.Stderr))

	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{"172.17.5.193:2379"},
		DialTimeout: time.Second * 1,
	})
	if err != nil {
		log.Fatal(err)
	}
	defer cli.Close() // make sure to close the client
	//_, err = cli.Put(context.TODO(), "wise", "2222")
	//res, err := cli.MemberList(context.TODO())
	res, err := cli.Get(context.TODO(), "wise")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(res.Kvs)
}
